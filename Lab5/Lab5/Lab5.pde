//*****************************************
// Henry(Haoyu) Wang
// CPMSC 382 fall 2015
// 10/20/2015
// Lab 5 - Image Manipulation
//******************************************


float[][][] filters = {
  //Emboss, need to add offset 127 as weights add up to 0
                        {{1, 1, 0}, 
                        {1, 0, -1}, 
                        {0, -1, -1}}};
float[][] ownFilters =  {
  //Create a my own filter combine Emboss and average
                       {1/6., -1/6.,1/6.},
                       {1/6., 1/6., 1/6.},
                       {0, -1, 0}};
                              
int counter = 0;

PImage img, img2, img3, img4;
//PFont font;

void setup() {
  
  img = loadImage("Lab.jpg");
  img2 = loadImage("Lab.jpg");
  img3 = loadImage("Lab.jpg");
  img4 = loadImage("Lab.jpg");
  size(2*img.width, 2*img.height);
  
  image(img, 0, 0);
  image(img2,0,img.height);
  image(img3, img.width, 0);
  image(img4,img.width,img.height);
  loadPixels();
  img2.loadPixels();
  img3.loadPixels();
  img4.loadPixels();
  image(img, 0, 0); 
  
  applyFilter();
   for (int i = 0; i < pixels.length; i++) {
    if ((i % (2*img.width)) >= img.width) {
      if ((i < (2*img2.width)*img2.height)) //make it for only right top corner
      {
      color c = pixels[i];
      float colorRed = red(c);
      float colorGreen = green(c);
      float colorBlue = blue(c);
      pixels[i] = color(colorRed/3, colorGreen, colorBlue/2);
      //System.out.println(colorRed + ", " + colorGreen + ", " + colorBlue);
    } //if
    }
  } //for
  updatePixels();
} //setup

void draw(){
   applyFilter();
   applyFilter1();
}

void applyFilter() {
  for (int y=0; y<img.height; y++ ) {
    for (int x=0; x<img.width; x++) {
      img2.pixels[y*img.width+x] = convolution(x, y, filters[counter%filters.length], img);
    }
  }
  img2.updatePixels();
  image(img2, 0, height/2);
  counter++;
}
void applyFilter1(){
  image(img4,img.width,img.height);
  img4.loadPixels();
  for(int y=0; y<img.height; y++){
    for(int x=0; x<img.width; x++){
      img4.pixels[y*img.width+x] = convolution(x, y, ownFilters, img);
    }
  }
  img4.updatePixels();
  counter++;
}  
// calculates the color after applying the filter
color convolution(int x, int y, float[][] matrix, PImage img) {
  int offset = floor(matrix.length/2);
  float r = 0.0, g = 0.0, b = 0.0;

  for (int i = 0; i < matrix.length; i++) {
    for (int j= 0; j < matrix[i].length; j++) {
      // Which neighbor are we using
      int newX = x+i-offset;
      int newY = y+j-offset;
      int idx = img.width*newY + newX;
      // Make sure we haven't walked off our image
      idx = constrain(idx, 0, img.pixels.length-1);
      // Calculate the convolution
      r += (red(img.pixels[idx]) * matrix[i][j]);
      g += (green(img.pixels[idx]) * matrix[i][j]);
      b += (blue(img.pixels[idx]) * matrix[i][j]);
    }
  }
  if (counter%filters.length == filters.length-1) { // last filter is the emboss filter, add offset
    return color(r+127, g+127, b+127);
  }
  else {
    return color(r, g, b);
  }
}


