//*****************************************
// Henry(Haoyu) Wang
// CPMSC 382 fall 2015
// 11/18/2015
// Lab 7 part2 - Choosing Optimal Colors - A Better Google Maps 
//******************************************
PImage img;
float h,s,v;
void setup(){
  img=loadImage("lab7part2.png");
  size(img.width, img.height);
  image(img, 0, 0);
  colorMode(HSB, 360, 1, 1);
  img.loadPixels();
  for(int i=0; i<img.pixels.length; i++){
      color c = img.pixels[i];
      float h = hue(c);
      float s = saturation(c);
      float v = brightness(c);
      img.pixels[i] = change(h, s, v);
  }
  img.updatePixels();
  image(img, 0, 0);
}

color change(float h, float s, float v){
  // change the background
  if(h>=39 && h<=42){
    s = s+0.3;
    v = v+0.3;
  }
  // change the street
 if(h>=0 && h<=10){
  s=s+0.3;
  v=v+0.3;
 }
 // change the park
 if(h>=82 && h<= 90){
   s=s+0.3;
   v=v+0.3;
 }
 // change the route
 if(h>=30 && h<=38){
   h = h-10;
   s=s+0.3;
   v=v+0.3;
  }
  // change the hotel color
  if(h>42 && h<=45 ){
    s=s+0.3;
    v=v-0.3;
  }
  // change the river
 if(h>=210 && h<=218){
   s = s+0.3;
   v = v+0.3;
 }
  return color(h, s, v);
}

void mousePressed(){
  img.loadPixels();
  int i = mouseY*width+mouseX;
  color c=img.pixels[i];
  float h=hue(c);
  float s=saturation(c);
  float v=brightness(c);
  System.out.println("h= " + h + " s= " + s + " v= " + v);
}

void draw(){
}
