//*****************************************
// Henry(Haoyu) Wang
// CPMSC 382 fall 2015
// 11/18/2015
// Lab 7 part1 - Choosing Optimal Colors - Desaturating an Image 
//******************************************
PImage img;
float h, s, v;
void setup(){
  img=loadImage("lab7part1.png");
  size(img.width, img.height);
 
  image(img, 0, 0);
  loadPixels();
  for(int i=0; i<pixels.length; i++){
      color c= pixels[i];
      float r=red(c)/255;
      float g = green(c)/255;
      float b = blue(c)/255;
      h=convertH(r, g, b);
      s=convertS(r, g, b);
      v=convertV(r, g, b);
      //System.out.println("h= " + h + " s= " + s + " v= " + v);
      pixels[i]=convertRGB(h, s, v);
      
  }
      updatePixels();
}


void draw(){
}

float convertH(float r, float g, float b){
  float Max= findMax(r, g, b);
  float Min = findMin(r, g, b);
  float h = 0;
  if(Max == Min){
    h=0;
  }
  else if(Max == r){
    h=(60*((g-b)/(Max-Min)))%360;
  }
  else if(Max == g){
    h=60*((b-r)/(Max-Min))+120; 
  }
  else if(Max == b){
    h=60*((r-g)/(Max-Min))+240;
  }
  return h;
}

float convertS( float r, float g, float b){
  float Max = findMax(r, g, b);
  float Min = findMin(r, g, b);
  float s=0;
  if(Max == 0){
    s=0;
  }
  else{
    s=1-(Min/Max);
  }
  return s;
}

float convertV(float r, float g, float b){
  float Max = findMax(r, g, b);
  float v = Max;
  return v;
}

float findMax(float r, float g, float b){
  float Max=0;
  if(r>=g && r>=b){
    Max = r;
  }
  else if(g>=r && g>=b){
    Max = r;
  }
  else if(b>=r && b>=g){
    Max = b;
  }
  return Max;
}

float findMin(float r, float g, float b){
  float Min = 0;
  if(r<=g && r<=b){
    Min = r;
  }
  else if(g<=r && g<=b){
    Min = g;
  }
  else if(b<=r && b<=g){
    Min = b;
  }
  return Min;
}

color convertRGB(float h, float s, float v){
  float r = 0;
  float g = 0;
  float b = 0;
  int h_i = ((int)(h/60))%6;
  float f = h/60-(int)(h/60);
  float p = v*(1-s);
  float q = v*(1-f*s);
  float t = v*(1-(1-f)*s);
  if(h_i == 0){
    r = v;
    g = t;
    b = q;
  }
  else if(h_i == 1){
    r = q;
    g = v;
    b = p;
  }
  else if(h_i == 2){
    r = p;
    g = t;
    b = v;
  }
  else if(h_i == 3){
    r = p;
    g = q;
    b = v;
  }
  else if(h_i == 4){
    r = t;
    g = p;
    b = v;
  }
  else if(h_i == 5){
    r = v;
    g = p;
    b = q;
  }
  
  return color(255*r, 255*g, 255*b);
}

