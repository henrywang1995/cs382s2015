//*****************************************
// Henry(Haoyu) Wang
// CPMSC 382 fall 2015
// 09/22/2015
// Lab 3 -Transformation Matrices and Their Usage
//******************************************



float deg = 25;
float dd = 0;
color bColor = color(255, 255, 255);
Rain r1;
int numDrops = 50;   //numbers of drops 
Rain[] drops = new Rain[numDrops]; // Declare and create the array
float r;
float y; 

void setup() {
  size(600, 650, P3D);
  frameRate(20);
  for (int i = 0; i < drops.length; i++) {
    drops[i] = new Rain(); // Create each object
   // r1 = new Rain();
}
}

void draw() {
  //deg = 45;
  //strokeWeight(1);
   
  background(bColor);
  line(width/2, height, width/2, height-200);
  pushMatrix(); 
  translate(width/2, height-200);
  //rotateY(radians(dd));
  rotate(PI);
  drawtree(50, 15);
  popMatrix();

  //noLoop();
  //dd++;

  for(int i=0; i<drops.length; i++){
    drops[i].update();
    drops[i].drawRain();
  } //for - rain 

}
 
void drawtree(float l, int num) {
   
  if(num>1) {
  //deg = deg - 1;
  pushMatrix();
  rotateY(radians(dd));
  rotate(radians(deg));
  line(0, 0, 0, l);
  translate(0, l);
   
  drawtree(l*0.9, num-1);
  popMatrix();
   
  pushMatrix();
  rotate(radians(-deg));
  rotateY(radians(dd));
  line(0, 0, 0, l);
  stroke(76);
  translate(0, l);
   
  drawtree(l*0.9, num-1);
  popMatrix();   
   
  }
  dd++;
   
}

void bcolor(){
  background(bColor);
}

void mousePressed() {
  bColor = color(random(255), random(255), random(255));
}

