//*****************************
//Henry(Haoyu) Wang
//CS382 Lab1 
// description: creat a rotating flower base 
//on float scalefactor and float angle function 
//*******************************

float initSize = 220;
float scaleFactor = 0.99995;
float speed=0.01;
float angle=0.50;

void setup() {
  size(900, 700);               // set up a retangle
  background(0, 200, 255);      //make a bule background
  noFill();
  strokeWeight(0.8);
  stroke(0,100,0);              
  // make stroke to  the dark green

}

void draw() {                                         
  translate(width/2, height/2);                
  rotate(-PI/2);
  for (int i = 0; i<30; i++) {                        //use a for loop  let rectanlge rotate infinite times
    float size = initSize*pow(scaleFactor, i);        
    rect(0, 0, size, size);                      
    rotate(PI/4);                              // rotate the rectanlge PI/4 times
    rotate(PI/12);                             // rotate the rectanlge PI/12 times
    rotate(PI/6);                              // rotate the rectanlge PI/6 times
    scaleFactor*=0.99995;
    
     angle = angle+0.1;                           
  rotate(angle);                               
  float radius=0.98;                          
  for (int deg=0;deg<360;deg+=10){
  float x=75+(1-cos(angle)*radius);
  float y=42-(1-sin(angle)*radius);
  ellipse(x,y,20,20);                      // create a ellipse with size(20,20) will rotating by the float anlge function
  ellipse(13,13,14,14);                    // create a ellipse with size(14,14) staying in the center.
  radius=radius+0.34;
  rotate(angle);
  }  
  }
}



  
