//*****************************************
// Henry(Haoyu) Wang
// CPMSC 382 fall 2015
// 10/06/2015
// Lab 4 -Lighting and Camera
//******************************************


float angleCubes=30; // angle for the cubes in the scene
CamClass cam;
float camX, camY, camZ;
float lookX, lookY, lookZ;
float angle, angleV;
PVector ballpos = new PVector (300, 300, -100);
PVector ballvel = new PVector (random(1, 2), random(-2, -1), random(-2, -1));
 
void setup() {
  size (850, 700, P3D);
  cam = new CamClass ();
  camX = 0;

} // fun
 
void draw() {
  // clear canvas
  background(15);
  // apply lights
  lights();
  // set the values of the class to the real camera
  cam.set();
  // move ball and draw it 
  ballpos.add(ballvel);
  fill(0, 255, 0);
  noStroke();
  myShape(ballpos.x, ballpos.y, ballpos.z, 9);
  // contain ball in a invisible box
  contain(ballpos, ballvel);
  // make the scene with the boxes
  scene();
  // text upper left corner
  fill(0, 255, 0);
  
  keyboardInput();
  
  cam.HUD_text("The green ball is having a journey in the 3D world!");
  //
}
void keyboardInput() {
  if (keyPressed) {
    if (keyCode == 'q');
    ambientLight(255, 255, 255);
    if (keyCode == 'w');
    directionalLight(0, 255, 112, 12, 10, 10);
    if (keyCode == 'e');
    pointLight(136, 144, 78, 100, 100, 102);
  } //if
}
void scene () {
  stroke(2);
  float z ;
  // one wall of boxes
  for (int x = 10; x < 600; x+= 60)
  {
    fill(x/3, 3, 3);
    for (int y = 10; y < 600; y+= 60)
    {
      z = -600;
      myBox(x, y, z, 24);
      // println ( "Box: " + x + ", "+y+ " "+z);
    }
    fill(0, 0, 254);
    z=-800;
    myBox(x, 20, z, 24);
  }
  // a few additional boxes
  fill(0, 0, 255);
  z=200;
  myBox(220, 10, z, 24);
  myBox(600, 10, z, 36);
  z=200;
  myBox(220, 510, z, 24);
  myBox(600, 510, z, 36);
  z=-200;
  myBox(220, 510, z, 24);
  myBox(600, 510, z, 36);
  z=-600;
  myBox(220, 510, z, 24);
  myBox(600, 510, z, 36);
  angleCubes++;
  //
}
 
void myBox(float x, float y, float z,float size1) {
  // one nice wrapper for build in box-command
  pushMatrix();
  translate(x, y, z);
  rotateY(radians(angleCubes));
  rotateX(radians(30));
  box(size1);
  popMatrix();
}
 
void myShape(float x, float y, float z, float size1) {
  // one nice wrapper for build in box-command
  pushMatrix();
  translate(x, y, z);
  rotateY(radians(angleCubes));
  rotateX(radians(30));
  sphere(size1);
  popMatrix();
}
 
void contain(PVector ballPos, PVector ballVel) {
  //
  fill(0, 255, 0);
  myBox(500, 500, 500, 20);
  myBox(300, 200, 300, 20);
  myShape(200, 300, 200, 20);
  myShape(100, 100, 100, 20);
  //
  if (ballpos.x>500)
    ballvel.x=abs(ballvel.x)*-1;
  if (ballpos.y>500)
    ballvel.y=abs(ballvel.y)*-1;
  if (ballpos.z>500)
    ballvel.z=abs(ballvel.z)*-1;
 
  if (ballpos.x<100)
    ballvel.x=abs(ballvel.x);
  if (ballpos.y<100)
    ballVel.y=abs(ballvel.y);
  if (ballpos.z<100)
    ballvel.z=abs(ballvel.z);
}
// 
 
class CamClass {
  // capsuled the normal camera() command and its vectors
  PVector camPos;     // its vectors
  PVector camLookAt;
  PVector camUp;
 
  PVector camPosInitial;     // its vectors - the default (unchanged)
  PVector camLookAtInitial;
  PVector camUpInitial;
 
  // for follow
  PVector camWhereItShouldBe = new PVector(0, 0, 0);
  PVector camAdd = new PVector(0, -60, 0);
  float easing = .09; // .07; // how fast it changes
 
  float camCurrentAngle=-90;   // for cam rotation around itself (around Y-axis)
  float camRadius;             // same situation
 
  // constructor without parameters
  CamClass() {
    // constr
    // set vectors
    camPos    = new PVector(width/2.0, height/2.0, 900);
    camLookAt = new PVector(width/2.0, height/2.0, -600);
    camUp     = new PVector( 0, 1, 0 );
    // save the initial values
    camPosInitial    = camPos.get();
    camLookAtInitial = camLookAt.get();
    camUpInitial     = camUp.get();
  }  // constr
 
  void set() {
    // apply vectors to actual camera
    camera (camPos.x, camPos.y, camPos.z,
    camLookAt.x, camLookAt.y, camLookAt.z,
    camUp.x, camUp.y, camUp.z);
  }
 
void SetLookAt (float x1, float y1, float z1) {
    camLookAt = new PVector(x1, y1, z1);
  }
 
void printData() {
    println ( "Cam at " + camPos
      + " looking at " + camLookAt
      + " (angle = "
      +camCurrentAngle
      +").");
}
   
void lookAtPVector (PVector follow) {
    // follows a player (e.g.)
    camLookAt = follow.get();
  }


void HUD_text (String a1) {
    // HUD text upper left corner
    // this must be called at the very end of draw()
 
    // this is a 2D HUD
    camera();
    hint(DISABLE_DEPTH_TEST);
    noLights();
    // ------------------
    textSize(16);
    text (a1, 20, 20);
    // ------------------
    // reset all parameters to defaults
    textAlign(LEFT, BASELINE);
    rectMode(CORNER);
    textSize(32);
    hint(ENABLE_DEPTH_TEST); // no HUD anymore
    lights();
  } // method
  //
} // class
