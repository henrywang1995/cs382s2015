class Tile {
  PImage img;
  int x, y, xStart, yStart;
  int originX, originY;
  float angle, spinRate;
  boolean moving;
  
  Tile(PImage i, int newX, int newY, int initialX, int initialY) {
    img = i;
    x = newX;
    y = newY;
    originX=initialX;
    originY=initialY;
    yStart = newY;
    xStart = newX;
    angle = 0;
    spinRate = random(3, 10);
    moving = false;
  } //Tile (constructor)
  
  void update() {
    angle = (angle + spinRate) % 360;
    
    if (moving==true) {
      if(x<originX){
        x++;
      }
      else if(x>originX){
        x--;
      }
      else{
        x=x;
      }
      if(y<originY){
        y++;
      }
      else if(y>originY){
        y--;
      }
      else{
        y=y;
      }
    } //if
    //System.out.println("originX= " + originX + "originY= " + originY);
    if ((originX==x) && (originY==y))
    {
      moving = false;
    } //if
  } //update
  
  void drawTile() {
    pushMatrix();
    translate(x+10, y+10);
    if (moving==true) {
      rotate(radians(angle));
    } //if
    translate(-10, -10);
    image(img, 0, 0);
    popMatrix();    
  } //drawTile
} //Tile (class)

