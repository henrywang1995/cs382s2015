//*****************************************
// Henry(Haoyu) Wang
// CPMSC 382 fall 2015
// 11/03/2015
// Lab 6 - Image Reassembly
//******************************************
PImage img;
Tile[] tiles;

void setup() {
  img = loadImage("lunar.jpg");
  size(img.width, img.height);
  //image(img, 0, 0);
  tiles = new Tile[288];
  
  int tileNum = 0;
  for (int y = 0; y < img.height; y += 20) {
    for (int x = 0; x < img.width; x += 20) {
      tiles[tileNum] = new Tile(img.get(x, y, 20, 20), (int)random(img.width), (int)random(img.height), x, y);
      tileNum++;
    } //for   
  } //for
  
} //setup
  
void draw() {
  background(0);
  if (mousePressed == true) {
  //int tileChoice = (int)random(0, 288);
  for(int tileChoice = 0; tileChoice < 288; tileChoice++){
    if (tiles[tileChoice].y == tiles[tileChoice].yStart && tiles[tileChoice].x == tiles[tileChoice].xStart ) {
      tiles[tileChoice].moving = true;
    } //if
  }
  }

  for (int i = 0; i < tiles.length; i++) {
    tiles[i].update();
    tiles[i].drawTile();
  } //for
} //draw

