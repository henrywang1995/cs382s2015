//*****************************************
// Henry(Haoyu) Wang
// CPMSC 382 fall 2015
// 12/08/2015
// Lab 8 - Basic Visualization Techniques
//******************************************
DotChart chart;
Table table;
ArrayList<Integer> days = new ArrayList<Integer>();
ArrayList<Integer> temps = new ArrayList<Integer>();
ArrayList<Integer> times = new ArrayList<Integer>();
float dataX, dataY;
int index;
float differenceX;
float differenceY;
void setup(){
  size(800, 400);
  background(255);
  //table = loadTable("beav1.csv", "header");
  table = loadTable("beavtime.csv", " header");
  System.out.println("The total number of rows: " +table.getRowCount());
  for (TableRow row : table.rows()){
    int day = row.getInt("day");
    //int temp = row.getInt("temp");
    int time = row.getInt("time");
    //System.out.println("The day: "+ day + " the temp: " + temp);
    days.add(day);
    //temps.add(temp); 
    times.add(time); 
  }
    chart = new DotChart(times, days);
    //chart = new DotChart(temps, days);
    chart.drawChart();
    
}
void mousePressed(){
  //System.out.println("MouseX= " + mouseX + "MouseY= " + mouseY);
  //System.out.println(chart.scaleX);
  //System.out.println(chart.scaleY);
   dataX = ((float)(mouseX-30)/75)*chart.scaleX;
   dataY = ((float)(height-mouseY-20)/30)*chart.scaleY;
   differenceX=0.1*chart.scaleX;
   differenceY=0.07*chart.scaleY;
   System.out.println("DataX = " + dataX + "DataY= " + dataY);
  if(check(dataX, dataY, days, times)==true){
    fill(255, 0, 0);
    textSize(10);
    System.out.println("The index value is: " + index);
    text("Time= " + times.get(index), mouseX, mouseY-20);
    text("DAY= " + roundTo(dataX), mouseX, mouseY-10);
   
   }
  // if(check(dataX, dataY, days, temps)==true){
    //fill(255, 0, 0);
    //textSize(10);
   // System.out.println("The index value is: " + index);
    //text("Temp = " + temps.get(index), mouseX, mouseY-20);
    //text("DAY = " + roundTo(dataX), mouseX, mouseY-10);
  // }
}
void mouseReleased(){
  background(255);
  chart = new DotChart(times, days);
  //chart = new DotChart(temps, days);
  chart.drawChart();
  

}
boolean check(float dataX, float dataY, ArrayList<Integer> data1, ArrayList<Integer> data2){
  for(int i =0; i<data1.size(); i++){
    if((((int)dataX>=data1.get(i)-differenceX && (int)dataX<=data1.get(i)+differenceX))&&((dataY>=data2.get(i)-differenceY) && (dataY<=data2.get(i)+differenceY))){
      System.out.println("It's true");
      System.out.println("Current X= " + dataX);
      System.out.println("expected X= " + data1.get(i));
      index = i;
      return true;
    }
  }
  return false;
}

int roundTo(float num){
  if(num-(int)num >=0.5){
    return ((int)num+1);
  }
  else
    return (int)num;
}

void draw(){
   
}
