class DotChart{
  ArrayList<Integer> dataY = new ArrayList<Integer>(); // Y axis dataset
  ArrayList<Integer> dataX = new ArrayList<Integer>(); // x axis dataset
  float r = 5;
  float g = 5;
  float b = 5;
  int scaleY=0;
  int scaleX=0;
  
  DotChart(ArrayList<Integer> dataY, ArrayList<Integer> dataX){
    this.dataY = dataY;
    this.dataX = dataX;
    
  }
  
  void drawChart(){
    strokeWeight(2);
    // drawing the x-y axis
    int maxY=findMax(dataY);
    int maxX=findMax(dataX);
    if(maxX%10 == 0){
      scaleX=maxX/10;  
    }
    else{
      scaleX=(int)maxX/10+1;
    }
    if(maxY%10 == 0){
      scaleY=maxY/10;
    }
    else{
      scaleY=(int)maxY/10;
    }
    System.out.println("scaleX" + scaleX + " scaleY" + scaleY);
    textSize(9);
    fill(r, g, b);
    text(0, 20, height-20);
    point(0, 30, height); // draw the y-axis
    for(int i = 1; i<=(int)(height/30); i=i+1){
      point(height, 3, height);  // draw the scale on the y-axis
      text(scaleY*i, 0 , height-20-i*30+9/2);
    }
    if (findMin(dataY)<0){
      point(height/2, width, height/2); // draw the x-axis
      for(int j =1; j<=(int)(width/80); j++){
        point(height/2+9/2,height/2+12);
        text(scaleX*j, 30+j*80-9/2 ,height/2+6 );
      }
    }
    else{
      point(height, width, height); // draw the x-axis
       for(int j =1; j<=(int)(width/75); j++){
        point(height,height);
        text(scaleX*(j), 30+(j)*75-9/2 ,height-6 );
      }
    }
    
    // ploting the points on the graph
    for(int a =0; a<dataY.size()-1; a++){
      float xValue = ((float)dataX.get(a))/scaleX;
      float yValue = ((float)dataY.get(a))/scaleY;
      //System.out.println("x= " + xValue + "y= " + yValue);
      float nxValue = ((float)dataX.get(a+1))/scaleX;
      float nyValue = ((float)dataY.get(a+1))/scaleY;
      //System.out.println("nx= " + nxValue + "ny= " + nyValue);
      strokeWeight(4);
      stroke(4, 98, 116);
      point(height-20-yValue*30 ,height-20-nyValue*30 );
      ellipse(30+xValue*75+0.5, height-20-yValue*30+0.5,2,2);
      ellipse(30+nxValue*75+0.5, height-20-nyValue*30+0.5,2,2);
      strokeWeight(2);
      stroke(0);
    }
   
    
  }
  // this method finds the minmum value of the data
  int findMin(ArrayList<Integer> data){
    int min=data.get(0);
    for(Integer i : data){
      if(min>=i){
        min=i;
      }
    }
    //System.out.println("min= " + min);
    return min;
  }
  // this method finds the maximum value of the data
  int findMax(ArrayList<Integer> data){
    int max=data.get(0);
    for(Integer i : data){
      if(max<=i){
        max=i;
      }
    }
    //System.out.println("max= " + max);
    return max;
  }
}
